"use strict";

function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return decodeURI(r[2]); return null;
}
let mydata = new trip();/* Initialising locally stored data */
mydata.restdata();/* Initialisation of reset data */
var localdata = localStorage.getItem('TS_CurrentTrip');
var maindata = JSON.parse(localdata)
console.log(maindata)
let routes = maindata.routes.map(a => a.name).join(" => ");
document.querySelector('.Scheduled').innerHTML = formatDate(maindata.startDateTime)
document.querySelector('.Total').innerHTML = getTotalDistance(maindata.routes)
document.querySelector('.origin').innerHTML = maindata.routes[0].name
document.querySelector('.destination').innerHTML = maindata.routes[maindata.routes.length - 1].name
document.querySelector('.Routes').innerHTML = routes
document.querySelector('.Number').innerHTML = maindata.routes.length;

let position = [];/* Store all coordinates */
let routerdata = [];/* Store all flight coordinates */
function getTotalDistance(routes) {
    let total = 0;
    if (routes.length > 1) {

        for (let i = 0; i < routes.length - 1; i++) {
            // console.log(this.routes[i])
            // console.log(this.routes[i + 1])
            let distance = calculateDistance(routes[i].latitude, routes[i].longitude, routes[i + 1].latitude, routes[i + 1].longitude);
            total += distance;
        }
    }

    return total.toFixed(2);
}
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}
mapboxgl.accessToken = accessToken;
var map = new mapboxgl.Map({
    container: 'map', // container ID
    style: 'mapbox://styles/mapbox/streets-v11', // style URL
    center: [GetQueryString('longitude'), GetQueryString('latitude')], // starting position [lng, lat]
    zoom: 4, // starting zoom
});

/* Map drawing marker points */
map.on('load', function () {
    let routes = maindata.routes;
    let coordinates = routes.map(a => [a.longitude, a.latitude]);
    drawRoute(coordinates);

    routes.forEach(element => {
        let popup = new mapboxgl.Popup({ closeOnClick: true })
            .setLngLat([element.longitude, element.latitude])
            .setText(element.name)
            .addTo(map);

        let marker = new mapboxgl.Marker()
            .setLngLat([element.longitude, element.latitude])
            .setPopup(popup)
            .addTo(map);
    });

});

/* Search all flight itineraries for the current airport */
// webServiceRequest(' https://eng1003.monash/OpenFlights/routes/', { 'sourceAirport': GetQueryString('airportId'), 'callback': 'showdatas' })

/* Search Airports */
function showairports(data) {
    console.log(data)
    data.forEach((ele) => {
        position.push({ 'position': [ele.longitude, ele.latitude], airportId: ele.airportId })
        /* Store the coordinates and the corresponding airport id */
    })
    console.log(position)
    /* Connections */
    map.on('load', function () {
        position.forEach((ele, i) => {
          
            addline(ele.position, i)
            // }

        })
    });

}
/* addline */
function addline(position, i) {
    map.addLayer({
        "id": `route${i}`,
        "type": "line",
        "source": {
            "type": "geojson",
            "data": {
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": [
                        position,
                        [GetQueryString('longitude'), GetQueryString('latitude')]
                    ]
                }
            }
        },
        "layout": {
            "line-join": "round",
            "line-cap": "round"
        },
        "paint": {
            "line-color": getRandomColor(),
            "line-width": 8
        }
    });
}
/*Generate random hexadecimal colour values */
function getRandomColor() {
    var str = "#";
    var arr = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"];
    for (var i = 0; i < 6; i++) {
        var num = parseInt(Math.random() * 16);
        str += arr[num];
    }
    return str;
}
function drawRoute(coordinates) {
    if (typeof map.getLayer('routeToView') !== 'undefined') {
        // Remove map layer & source.
        map.removeLayer('routeToView').removeSource('routeToView');
    }

    map.addSource('routeToView', {
        'type': 'geojson',
        'data': {
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'LineString',
                'coordinates': coordinates
            }
        }
    }
    );

    map.addLayer({
        'id': 'routeToView',
        'type': 'line',
        'source': 'routeToView',
        'layout': {
            'line-join': 'round',
            'line-cap': 'round'
        },
        'paint': {
            'line-color': "#FF0020",
            'line-width': 5
        }
    });
}
document.querySelector('.addplan').addEventListener('click', function () {
    var username = document.querySelector('.username').value;
    console.log(username)
    if (username != '') {
        let adddata = { ...maindata, 'username': username }
        mydata.adddata(adddata)/* Additional data */
        mydata.savelocal() /* Storing data*/
        console.log(adddata);
        // return false;

        alert('add success');
        window.location.href = './myplan.html'
    }
})
function calculateDistance(lat1, long1, lat2, long2) {

    //radians
    lat1 = (lat1 * 2.0 * Math.PI) / 60.0 / 360.0;
    long1 = (long1 * 2.0 * Math.PI) / 60.0 / 360.0;
    lat2 = (lat2 * 2.0 * Math.PI) / 60.0 / 360.0;
    long2 = (long2 * 2.0 * Math.PI) / 60.0 / 360.0;


    // use to different earth axis length    
    var a = 6378137.0;        // Earth Major Axis (WGS84)    
    var b = 6356752.3142;     // Minor Axis    
    var f = (a - b) / a;        // "Flattening"    
    var e = 2.0 * f - f * f;      // "Eccentricity"      

    var beta = (a / Math.sqrt(1.0 - e * Math.sin(lat1) * Math.sin(lat1)));
    var cos = Math.cos(lat1);
    var x = beta * cos * Math.cos(long1);
    var y = beta * cos * Math.sin(long1);
    var z = beta * (1 - e) * Math.sin(lat1);

    beta = (a / Math.sqrt(1.0 - e * Math.sin(lat2) * Math.sin(lat2)));
    cos = Math.cos(lat2);
    x -= (beta * cos * Math.cos(long2));
    y -= (beta * cos * Math.sin(long2));
    z -= (beta * (1 - e) * Math.sin(lat2));

    return (Math.sqrt((x * x) + (y * y) + (z * z)) / 10);
}
