"use strict";
let allRoutesFromAirport = [];/* Store airport information  */
/*map */
mapboxgl.accessToken = accessToken;
let map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/streets-v11",
    zoom: 4,
    center: [134.092090, -26.083647]
});
let currentTrip = new travel();
const queryString_allRoutes = "https://eng1003.monash/OpenFlights/allroutes/";
const queryString_allAirports = "https://eng1003.monash/OpenFlights/airports/";
const key_airportsInSelectedCountry = "TS_AirportsInSelectedCountry";
const key_allRoutesInSelectedCountry = "TS_AllRoutesInSelectedCountry";
const key_currentTrip = "TS_CurrentTrip";
const key_user = "TS_User";
const key_tripToViewId = "TS_TripToViewId";
let airportdata = [];/* Store airport data  */
let features = [];
/* Map drawing  */
let geojsonMarkers = {
    'type': 'geojson',
    'data': {
        'type': 'FeatureCollection',
        'features': features
    }
};
let routesFeatures = []

let geojsonRoutes = {
    'type': 'geojson',
    'data': {
        'type': 'FeatureCollection',
        'features': routesFeatures
    }
};
let selectedAirports = []
let geojsonSelectedRoutes = {
    'type': 'geojson',
    'data': {
        'type': 'Feature',
        'properties': {},
        'geometry': {
            'type': 'LineString',
            'coordinates': selectedAirports
        }
    }
}
/* Date change, update time  */
document.getElementById("choosetime2").addEventListener("change", function (e) {
    let date = document.getElementById("choosetime2").value;
    currentTrip.startDateTime = new Date(date);
    updateLocalStorage(key_currentTrip, currentTrip);
});
/* Click to search country information  */
document.querySelector('.citysubmit').addEventListener("click", function (e) {
    var choosecity = document.querySelector('#city').value;
    console.log(choosecity)
    /* Country information entered  */
    if (choosecity != "" && choosecity != undefined) {
        webServiceRequest(queryString_allAirports, { "country": choosecity, "callback": "onGetAllAirportsByCountryResponse" });
        webServiceRequest(queryString_allRoutes, { "country": choosecity, "callback": "onGetAllRoutesByCountryResponse" });
    } else {
        alert("Please select a country to start searching.")
    }
});

function getAllRoutesByAirport(airport) {
    webServiceRequest(' https://eng1003.monash/OpenFlights/routes/', { 'sourceAirport': airport, 'callback': 'onGetAllRoutesByAirportResponse' })
}
function onGetAllRoutesByAirportResponse(response) {
    routesFeatures = [];
    allRoutesFromAirport = response;
    let allRoutesInSelectedCountry = JSON.parse(localStorage.getItem(key_airportsInSelectedCountry));
    response.forEach(route => {
        let sourceAirport = allRoutesInSelectedCountry.find(r => r.airportId == route.sourceAirportId);
        let destinationAirport = allRoutesInSelectedCountry.find(r => r.airportId == route.destinationAirportId);
        //international airports are not considered
        if (destinationAirport != undefined && sourceAirport != undefined) {
            let feature = {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': [
                        [parseFloat(sourceAirport.longitude), parseFloat(sourceAirport.latitude)],
                        [parseFloat(destinationAirport.longitude), parseFloat(destinationAirport.latitude)]
                    ]
                }
            };
            routesFeatures.push(feature);
        }
    });
    geojsonRoutes.data.features = routesFeatures;
    drawAvailableRoutsFromAirport(geojsonRoutes);
}
function onGetAllRoutesByCountryResponse(response) {
    localStorage.setItem(key_allRoutesInSelectedCountry, JSON.stringify(response));
}


function onGetAllAirportsByCountryResponse(response) {
    airportdata = response;
    response.forEach((airport, i) => {
        if (i == 0) {
            /* Redirect the center point position  */
            map.flyTo({ center: [airport.longitude, airport.latitude] });
        }
        let feature = {
            type: "Feature",
            properties: {
                description: airport.name,
                iconSize: [25, 25],
                airport: airport
            },
            geometry: {
                type: "Point",
                coordinates: [parseFloat(airport.longitude), parseFloat(airport.latitude)]
            }
        }
        features.push(feature);
        geojsonMarkers.data.features = features;

        drawMarkers(geojsonMarkers);
    });

}

function isSourceAiport(routes) {
    if (routes != undefined && routes.length != 0) {
        return true;
    }
    return false;
}

function hasRouteInBetween(sourceAiport, destinationAirport) {
    if (sourceAiport == undefined || destinationAirport == undefined) {
        return false;
    } else {
        let allRoutesInSelectedCountry = JSON.parse(localStorage.getItem(key_allRoutesInSelectedCountry));
        console.log()
        let route = allRoutesInSelectedCountry.find(r => r.sourceAirportId == sourceAiport.airportId && r.destinationAirportId == destinationAirport.airportId);

        if (route != undefined) {
            return true;
        }
    }

    return false;
}

function showMenu() {
    let option1 = " A. Set as source airport\n";
    let option2 = " B. Set as a stop \n";
    let option3 = " C. Set as destination airport \n";
    let option4 = " D. Remove from current trip\n";
    let choice = prompt(option1 + option2 + option3 + option4);
    return choice;
}

function drawAvailableRoutsFromAirport(geojson) {
    if (typeof map.getLayer('routes') !== 'undefined') {
        map.removeLayer('routes').removeSource('routes');
    }

    map.addSource('routes', geojson);

    map.addLayer({
        'id': 'routes',
        'type': 'line',
        'source': 'routes',
        'layout': {
            'line-join': 'round',
            'line-cap': 'round'
        },
        'paint': {
            'line-color': "#888",
            'line-width': 3
        }
    });
}

function drawMarkers(geojson) {
    if (typeof map.getLayer('markers') !== 'undefined') {
        map.removeLayer('markers').removeSource('markers');
    }

    map.addSource('markers', geojson);

    map.addLayer({
        'id': 'markers',
        'type': 'circle',
        'source': 'markers',
        'paint': {
            'circle-color': '#4264fb',
            'circle-radius': 12,
            'circle-stroke-width': 2,
            'circle-stroke-color': '#ffffff'
        }
    });
}

function clearSelecteRoute() {
    if (typeof map.getLayer('selectedRoute') !== 'undefined') {
        // Remove map layer & source.
        map.removeLayer('selectedRoute').removeSource('selectedRoute');
    }
}

function drawSelectedRoute(geojson) {
    if (typeof map.getLayer('selectedRoute') !== 'undefined') {
        // Remove map layer & source.
        map.removeLayer('selectedRoute').removeSource('selectedRoute');
    }

    map.addSource('selectedRoute', geojson);

    map.addLayer({
        'id': 'selectedRoute',
        'type': 'line',
        'source': 'selectedRoute',
        'layout': {
            'line-join': 'round',
            'line-cap': 'round'
        },
        'paint': {
            'line-color': getRandomColor(),
            'line-width': 5
        }
    });
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}
map.on('click', 'markers', function (e) {
    var coordinates = e.features[0].geometry.coordinates.slice();
    var description = e.features[0].properties.description;
    var airport = JSON.parse(e.features[0].properties.airport);


    // Ensure that if the map is zoomed out such that multiple
    // copies of the feature are visible, the popup appears
    // over the copy being pointed to.
    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    new mapboxgl.Popup()
        .setLngLat(coordinates)
        .setHTML(description)
        .addTo(map);

    getAllRoutesByAirport(airport.airportId);

    //Only show menu when there is a route between the last airport in trip and the clicked airport 
    setTimeout(function () {
        // if ((currentTrip.getNumberOfStops() == 0 && isSourceAiport(allRoutesFromAirport)) || hasRouteInBetween(currentTrip.getLastAirport(), airport)) {
        setTimeout(function () {
            let choice = showMenu();

            if (['A', 'B', 'C', 'D'].includes(choice)) {

                if (choice == 'A' && currentTrip.getNumberOfStops() > 0) {
                    if (confirm("This will clear the current trip and start setting the trip from beginning. Do you want to continue?")) {
                        currentTrip.removeAllRoutes();
                        selectedAirports = [];
                    }
                }

                if (choice != 'D') {
                    currentTrip.addRoute(new Airport(airport.airportId, airport.name, airport.country, airport.latitude, airport.longitude));
                    selectedAirports.push([airport.longitude, airport.latitude]);
                } else {
                    if (currentTrip.getNumberOfStops() > 0) {
                        let lastAirport = currentTrip.getLastAirport();
                        if (lastAirport.airportId == airport.airportId) {
                            currentTrip.removeLastRoute();
                            selectedAirports.pop();
                        } else {
                            alert("Only last route can be removed from the trip. ");
                        }
                    } else {
                        alert("Nothing is removed as there is no stop in the current trip yet.");
                    }

                }

                geojsonSelectedRoutes.data.geometry.coordinates = selectedAirports;
                drawSelectedRoute(geojsonSelectedRoutes);

                if (choice == 'C') {
                    clearAvailableRoutesFromAirport();
                    console.log(currentTrip)

                    if (confirm(`Would you like to save this trip?`)) {
                        console.log(currentTrip.routes[0].name);
                        var result = airportdata.filter((ele) => {
                            return ele.name == currentTrip.routes[0].name;
                        })
                        currentTrip.tripId = user.getNextTripId();
                        updateLocalStorage(key_currentTrip, currentTrip);
                        user.addTrip(currentTrip);
                        updateLocalStorage(key_user, user);
                        localStorage.setItem(key_tripToViewId, currentTrip.tripId);
                        var choosecity = document.querySelector('#city').value;
                        var url = `./airportdetail.html?airportId=${result[0].airportId}&name=${result[0].name}&city=${result[0].city}&country=${result[0].country}&latitude=${result[0].latitude}&longitude=${result[0].longitude}&altitude=${result[0].altitude}&choosecity=${choosecity}
                `
                        console.log(url);
                        // return false;
                        window.location.href = url;
                        return false;
                        window.location = "./airportdetail.html";
                    }
                    else {
                        updateLocalStorage(key_currentTrip, new travel());
                        clearSelecteRoute();
                    }
                }
            }
        }, 400);
        // }
    }, 400);
});

function clearAvailableRoutesFromAirport() {
    if (typeof map.getLayer('routes') !== 'undefined') {
        map.removeLayer('routes').removeSource('routes');

    }
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

//Code on page load
document.getElementById("choosetime2").value = formatDate(new Date());
document.getElementById("choosetime2").setAttribute("min", formatDate(new Date()));

/*Generate random hexadecimal color values  */
function getRandomColor() {
    var str = "#";
    var arr = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"];
    for (var i = 0; i < 6; i++) {
        var num = parseInt(Math.random() * 16);
        str += arr[num];
    }
    return str;
}
