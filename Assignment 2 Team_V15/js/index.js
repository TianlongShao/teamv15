"use strict";

let routes = [];/* Global storage of country route data */
let mydata = new trip();/* Initialising locally stored data */
mydata.restdata();/* Initialisation of reset data */
citylist()
/* Country List Loading*/

function citylist() {
    var html = '';
    countryData.forEach((ele) => {
        html += `
                    <option value='${ele}'>${ele}</option>
                `
    })
    document.querySelector('#city').innerHTML = html;
    // document.querySelector('#country').innerHTML = html;
}

function showdata(data) {
    routes = data;
    console.log(data)
    var html = ' ';
    var time = document.querySelector('#choosetime').value;
    data.forEach((ele, i) => {
        var url = `./routesdetail.html?airline=${ele.airline}&airlineId=${ele.airlineId}&codeshare=${ele.codeshare}&destinationAirport=${ele.destinationAirport}&destinationAirportId=${ele.destinationAirportId}&equipment=${ele.equipment}&sourceAirport=${ele.sourceAirport}&sourceAirportId=${ele.sourceAirportId}&time=${time}
                `
        html += ` <tr>
                    <th>${ele.airline}</th>
                    <th>${ele.airlineId}</th>
                    <th>${ele.sourceAirport}</th>
                    <th>${ele.sourceAirportId}</th>
                    <th>${ele.destinationAirport}</th>
                    <th>${ele.destinationAirportId}</th>
                    <th>
                        <a href='javascript:;' class='addplan' onclick='addplan(${i})'>add plan</a>
                        <a href='${url}' class='detail' >show detail</a>
                    </th>
                </tr>
                `
    });
    document.querySelector('#Recommendedbox').innerHTML = html;
}

function addplan(i) {
    var time = document.querySelector('#choosetime').value;
    var username = document.querySelector('#username').value;
    if (time == '' || username == '') {
        return false;
    }
    mydata.adddata({ ...routes[i], 'time': time, 'username': username })/* Additional data */
    mydata.savelocal() /* Storing data */
    alert('add success');
}