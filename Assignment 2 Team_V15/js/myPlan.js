"use strict";

let localdata = new trip();
localdata.restdata();
let setdata = [];
let sumdata = [];

document.querySelector('button').addEventListener('click', function () {
    var username = document.querySelector('input[type="text"]').value;
    showdata(localdata.showdata(), username)

})

function showdata(localdata, username) {
    console.log(localdata)
    localdata = localdata.sort(dateData("time", true))
    var reultdata = localdata.filter((ele) => {
        return ele.username == username;
    })
    setdata = localdata;
    sumdata = localdata;
    console.log(localdata)
    maindata(reultdata)
}

function maindata(setdata) {
    console.log(setdata)
    var html = '';
    setdata.forEach((ele, i) => {
        let routes = ele.routes.map(a => a.name).join(" => ");
        html += ` <tr>
                <th>${formatDate(ele.startDateTime)}</th>
                <th>${getTotalDistance(ele.routes)}</th>
                <th>${ele.routes[0].name}</th>
                <th>${ele.routes[ele.routes.length - 1].name}</th>
                <th>${routes}</th>
                <th>${ele.routes.length}</th>
                <th>
                <a href='javascript:;' class='deleted' onclick='deleted(${i})'>delete data</a>
                <a href='javascript:;' class='detail' onclick='detail(${i})'>show detail</a>
                </th>
                </tr>
            `
    });
    document.querySelector('tbody').innerHTML = html;
}

function detail(i) {
    console.log(setdata[i]);
    console.log(JSON.stringify(setdata[i]));
    localStorage.setItem('TS_CurrentTrip', JSON.stringify(setdata[i]))
    setTimeout(function () {
        window.location.href = './airportdetail.html'
    }, 500)
    console.log(setdata[i])
}

function dateData(property, bol) { //property is the key you need to sort in, bol is ascending if true, false is descending
    return function (a, b) {
        var value1 = a[property];
        var value2 = b[property];
        if (bol) {
            //ascending order
            return Date.parse(value1) - Date.parse(value2);
        } else {
            //Descending order
            return Date.parse(value2) - Date.parse(value1)
        }

    }
}

document.querySelector('.all').addEventListener('click', function () {

    setdata = sumdata;
    console.log(setdata)
    maindata(setdata)
})

document.querySelector('.history').addEventListener('click', function () {
    var result = sumdata.filter((ele) => {
        return new Date(ele.startDateTime).getTime() <= new Date().getTime()
    })
    maindata(result)
})

document.querySelector('.future').addEventListener('click', function () {
    var result = sumdata.filter((ele) => {
        return new Date(ele.startDateTime).getTime() > new Date().getTime()
    })
    maindata(result)
})

function deleted(i) {
    localdata.deleteddata(i)
    localdata.savelocal()
    var username = document.querySelector('input[type="text"]').value;
    showdata(localdata.showdata(), username)
}

function calculateDistance(lat1, long1, lat2, long2) {

    //radians
    lat1 = (lat1 * 2.0 * Math.PI) / 60.0 / 360.0;
    long1 = (long1 * 2.0 * Math.PI) / 60.0 / 360.0;
    lat2 = (lat2 * 2.0 * Math.PI) / 60.0 / 360.0;
    long2 = (long2 * 2.0 * Math.PI) / 60.0 / 360.0;


    // use to different earth axis length    
    var ma = 6378137.0;        // Earth Major Axis (WGS84)    
    var b = 6356752.3142;     // Minor Axis    
    var f = (ma - b) / ma;        // "Flattening"    
    var ecc = 2.0 * f - f * f;      // "Eccentricity"      

    var beta = (ma / Math.sqrt(1.0 - ecc * Math.sin(lat1) * Math.sin(lat1)));
    var cos = Math.cos(lat1);
    var x = beta * cos * Math.cos(long1);
    var y = beta * cos * Math.sin(long1);
    var z = beta * (1 - ecc) * Math.sin(lat1);

    beta = (ma / Math.sqrt(1.0 - ecc * Math.sin(lat2) * Math.sin(lat2)));
    cos = Math.cos(lat2);
    x -= (beta * cos * Math.cos(long2));
    y -= (beta * cos * Math.sin(long2));
    z -= (beta * (1 - ecc) * Math.sin(lat2));

    return (Math.sqrt((x * x) + (y * y) + (z * z)) / 10);
}