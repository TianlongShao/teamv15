"use strict";
function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return decodeURI(r[2]); return null;
}

let mydata = new trip();/* Initialize local storage data  */
mydata.restdata();/* Initialize reset data  */
document.querySelector('.airline').innerHTML = GetQueryString('airline')
document.querySelector('.airlineId').innerHTML = GetQueryString('airlineId')
document.querySelector('.codeshare').innerHTML = GetQueryString('codeshare')
document.querySelector('.destinationAirport').innerHTML = GetQueryString('destinationAirport')
document.querySelector('.destinationAirportId').innerHTML = GetQueryString('destinationAirportId')
document.querySelector('.equipment').innerHTML = GetQueryString('equipment')
document.querySelector('.sourceAirport').innerHTML = GetQueryString('sourceAirport')
document.querySelector('.sourceAirportId').innerHTML = GetQueryString('sourceAirportId')

document.querySelector('.addplan').addEventListener('click', function () {
    var time = document.querySelector('#choosetime').value;
    var username = document.querySelector('#username').value;
    if (time == '' || username == '') {
        alert('pleace input time and username')
        return false;
    }
    let adddata = {
        'airline': GetQueryString('airline'),
        'airlineId': GetQueryString('airlineId'),
        'codeshare': GetQueryString('codeshare'),
        'destinationAirport': GetQueryString('destinationAirport'),
        'destinationAirportId': GetQueryString('destinationAirportId'),
        'equipment': GetQueryString('equipment'),
        'sourceAirport': GetQueryString('sourceAirport'),
        'sourceAirportId': GetQueryString('sourceAirportId'),
        'time': time,
        'username': username,
    }
    mydata.adddata(adddata)/* Append data  */
    mydata.savelocal() /* Storing data  */
    alert('add success');
    window.location.href = './home.html'
})