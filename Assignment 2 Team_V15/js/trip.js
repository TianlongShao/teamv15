"use strict";
// Class for the adding or removing trips.

class trip {
    constructor(data) {
        this._data = data;
    }
    get data() {
        return this._data
    }
    set data(data) {
        this._data = data;
    }
    showdata() {
        return this._data;
    }
    adddata(adddata) {
        this._data.push(adddata);
    }
    deleteddata(index) {
        this._data.splice(index, 1);
    }
    savelocal() {
        localStorage.setItem('user', JSON.stringify(this._data));
    }
    restdata() {
        var localdata = localStorage.getItem('user');

        if (localdata == null || localdata == '[]') {
            this._data = [];
        } else {
            this._data = JSON.parse(localdata);
        }
    }
}

// Class for showing airports.

class Airport {
    constructor(airportId, name, country, latitude, longitude) {
        this.airportId = airportId;
        this.name = name;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}

// Class for travel plans.

class travel {
    constructor() {
        this.tripId = 0;
        this.startDateTime = new Date();
        //contains a list of airports and each adjacent pair represents a route
        this.routes = [];
    }
    getTotalDistance() {
        let total = 0;
        if (this.routes.length > 1) {
            for (let i = 0; i < this.routes.length - 1; i++) {
                let distance = calculateDistance(this.routes[i].latitude, this.routes[i].longitude, this.routes[i + 1].latitude, this.routes[i + 1].longitude);
                total += distance;
            }
        }
        return total.toFixed(2);
    }
    getAllRoutes() {
        return this.routes;
    }
    getLastAirport() {
        return this.routes[this.routes.length - 1];
    }
    getNumberOfStops() {
        return this.routes.length;
    }
    addRoute(airport) {
        this.routes.push(airport);
    }
    removeAllRoutes() {
        this.routes = [];
    }
    removeLastRoute() {
        return this.routes.pop();
    }
    fromData(data) {
        let trip = new Trip();
        trip.routes = data.routes;
        trip.startDateTime = data.startDateTime;
        trip.tripId = data.tripId;
        return trip;
    }
}

// Class for storing trip under user id.

class User {
    constructor(userId) {
        this.userId = userId;
        this.trips = [];
    }
    getNextTripId() {
        if (this.trips.length > 0) {
            return parseInt(this.trips[this.trips.length - 1].tripId) + 1;
        } else {
            return 0;
        }
    }
    getNumberOfTrips() {
        return this.trips.length;
    }
    getAllTrips() {
        return this.trips;
    }
    addTrip(trip) {
        this.trips.push(trip);
    }
    removeTrip(tripId) {
        let tripToRemove = this.trips.findIndex(t => t.tripId == tripId);
        if (tripToRemove != undefined) {
            this.trips.splice(tripToRemove, 1);
        }
    }
    fromData(data) {
        let user = new User();
        let trips = []
        data.trips.forEach(element => {
            trips.push(new Trip().fromData(element));
        });
        user.trips = trips;
        return user;
    }
}

// Function for updating the local storage.

function updateLocalStorage(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
}

// Function for formatting date of trip

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

// Function for finding the total distance of the route.

function getTotalDistance(routes) {
    let total = 0;
    if (routes.length > 1) {

        for (let i = 0; i < routes.length - 1; i++) {
            let distance = calculateDistance(routes[i].latitude, routes[i].longitude, routes[i + 1].latitude, routes[i + 1].longitude);
            total += distance;
        }
    }

    return total.toFixed(2);
}

// Function for finding the next day's date

function tomorrow() {
    const today = new Date()
    const tomorrow = new Date(today)
    tomorrow.setDate(tomorrow.getDate() + 1)
    return tomorrow;
}

let user = new User();