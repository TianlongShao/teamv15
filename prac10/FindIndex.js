/* Write your function for implementing the Find Index algorithm here */

function checkArray(house){
    if (house.emergency === true){
        return house.address;
    }
}

function findIndexTest(array){
    let index = array.findIndex(checkArray);
    if (index === -1){
        return null;
    } else {
        return array[index].house;
    }
}
